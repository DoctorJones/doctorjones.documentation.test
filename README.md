
# Adding new columns #

## Introduction ##

> ⚠️ **Important**: When adding a new column, you will need to add the new column as the last column before the `ModifiedDate` and `BitArray` columns.

Adding a new column is not as simple as perfoming an `ALTER TABLE` statement, as there is no way of adding a column into the middle of a table.  Instead, we must do the following:

- Script up a new table
- Insert data from the current table into the new table
- Drop the current table
- Rename the new table to the name of the current table

We must do all of this in a transaction to ensure we do not lose any data, therefore your SQL will follow this basic template:

---
```sql
	USE [XpressWeigh]
	GO
	SET ANSI_NULLS ON
	GO
	SET QUOTED_IDENTIFIER ON
	GO

	BEGIN TRY
		BEGIN TRANSACTION;

		CREATE TABLE [dbo].[New_Table](
			[Id] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
			[IsDeleted] [bit] NOT NULL,
			[New_Column] [bigint] NULL,
			[ModifiedDate] [datetime] NULL,
			[BitArray] [bigint] NOT NULL,
		 CONSTRAINT [PK_New_Table] PRIMARY KEY CLUSTERED 
		(
			[Id] ASC
		)WITH (PAD_INDEX = ON, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF)
		)

		ALTER TABLE [dbo].[New_Table] ADD  CONSTRAINT [DF_New_Table_Id]  DEFAULT (newid()) FOR [Id]

		ALTER TABLE [dbo].[New_Table] ADD  CONSTRAINT [DF_New_Table_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]

		ALTER TABLE [dbo].[New_Table] ADD  CONSTRAINT [DF_New_Table_BitArray]  DEFAULT ((0)) FOR [BitArray]	

		INSERT INTO [dbo].[New_Table]
			   ([Id]
			   ,[IsDeleted]
			   ,[ModifiedDate]
			   ,[BitArray])
		 SELECT [Id]
			   ,[IsDeleted]
			   ,[ModifiedDate]
			   ,[BitArray]
			   FROM [dbo].[Old_Table]

		-- drop the existing table
		DROP TABLE [dbo].[Old_Table]

		-- rename the new one to replace the old one
		EXEC sp_rename '[dbo].[New_Table]', 'Old_Table'

		COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
		THROW
	END CATCH
```

---

As you can see, everything is done within a transaction, and if we have any failures, we roll back.

## Step by step ##

Firstly you will need to connect to the database in SQL Management studio.

Right click on the table you wish to modify, and select the following options: `Script Table as` ➜ `CREATE To` ➜ `New Query Editor Window`

You will then have a script that looks like this:

---
```sql
	USE [XpressWeigh]
	GO
	SET ANSI_NULLS ON
	GO

	SET QUOTED_IDENTIFIER ON
	GO

	CREATE TABLE [dbo].[System_Member_Discount_Type_Definition](
		[Id] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
		[Description] [nvarchar](50) NULL,
		[IsDeleted] [bit] NOT NULL,
		[ModifiedDate] [datetime] NULL,
		[BitArray] [bigint] NOT NULL,
	 CONSTRAINT [PK_System_Member_Discount_Type_Definition] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = ON, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
	) ON [PRIMARY]
	GO

	ALTER TABLE [dbo].[System_Member_Discount_Type_Definition] ADD  CONSTRAINT [DF_System_Member_Discount_Type_Definition_Id]  DEFAULT (newid()) FOR [Id]
	GO

	ALTER TABLE [dbo].[System_Member_Discount_Type_Definition] ADD  CONSTRAINT [DF_System_Member_Discount_Type_Definition_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
	GO

	ALTER TABLE [dbo].[System_Member_Discount_Type_Definition] ADD  CONSTRAINT [DF_System_Member_Discount_Type_Definition_BitArray]  DEFAULT ((0)) FOR [BitArray]
	GO

	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'When is set to true, this identifies that the record is marked as being deleted and is to be excluded from any query results' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'System_Member_Discount_Type_Definition', @level2type=N'COLUMN',@level2name=N'IsDeleted'
	GO

	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The date and time the record was last modified or changes syncronised from an XpressWeigh kit' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'System_Member_Discount_Type_Definition', @level2type=N'COLUMN',@level2name=N'ModifiedDate'
	GO

	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Any column that has been altered but not synchronised with the permanent consultant the column ordinal is stored in the BitArray' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'System_Member_Discount_Type_Definition', @level2type=N'COLUMN',@level2name=N'BitArray'
	GO
```

---

Change any occurences of the old table name to a temporary new table name.  I'd recommend using a `_new` suffix, so `System_Member_Discount_Type_Definition` becomes `System_Member_Discount_Type_Definition_new`.

Also remove the extended properties, these aren't necessary.

> ⚠️ **Important**: make sure the constraint names are unique and are being applied to the new table.  It is best practice to use the table name and column name within the constraint, to ensure uniqueness.

You will now end up with a script that looks like this:

---
```sql
	USE [XpressWeigh]
	GO
	SET ANSI_NULLS ON
	GO

	SET QUOTED_IDENTIFIER ON
	GO

	CREATE TABLE [dbo].[System_Member_Discount_Type_Definition_new](
		[Id] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
		[Description] [nvarchar](50) NULL,
		[IsDeleted] [bit] NOT NULL,
		[NewColumn] [bigint] NULL, -- ⬅⬅⬅ **NEW COLUMN IS HERE**
		[ModifiedDate] [datetime] NULL,
		[BitArray] [bigint] NOT NULL,
	 CONSTRAINT [PK_System_Member_Discount_Type_Definition_new] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = ON, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
	) ON [PRIMARY]
	GO

	ALTER TABLE [dbo].[System_Member_Discount_Type_Definition_new] ADD  CONSTRAINT [DF_System_Member_Discount_Type_Definition_new_Id]  DEFAULT (newid()) FOR [Id]
	GO

	ALTER TABLE [dbo].[System_Member_Discount_Type_Definition_new] ADD  CONSTRAINT [DF_System_Member_Discount_Type_Definition_new_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
	GO

	ALTER TABLE [dbo].[System_Member_Discount_Type_Definition_new] ADD  CONSTRAINT [DF_System_Member_Discount_Type_Definition_new_BitArray]  DEFAULT ((0)) FOR [BitArray]
	GO
```

---

Add the newly modified create script into our transaction template, and adjust the `INSERT` and `SELECT` statements accordingly.

You should be left with something like this:

---
```sql
	USE [XpressWeigh]
	GO
	SET ANSI_NULLS ON
	GO
	SET QUOTED_IDENTIFIER ON
	GO

	BEGIN TRY
		BEGIN TRANSACTION;

		CREATE TABLE [dbo].[System_Member_Discount_Type_Definition_new](
			[Id] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
			[Description] [nvarchar](50) NULL,
			[IsDeleted] [bit] NOT NULL,
			[NewColumn] [bigint] NULL, -- ⬅⬅⬅ **NEW COLUMN IS HERE**
			[ModifiedDate] [datetime] NULL,
			[BitArray] [bigint] NOT NULL,
		 CONSTRAINT [PK_System_Member_Discount_Type_Definition_new] PRIMARY KEY CLUSTERED 
		(
			[Id] ASC
		)WITH (PAD_INDEX = ON, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
		) ON [PRIMARY]
		GO

		ALTER TABLE [dbo].[System_Member_Discount_Type_Definition_new] ADD  CONSTRAINT [DF_System_Member_Discount_Type_Definition_new_Id]  DEFAULT (newid()) FOR [Id]
		GO

		ALTER TABLE [dbo].[System_Member_Discount_Type_Definition_new] ADD  CONSTRAINT [DF_System_Member_Discount_Type_Definition_new_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
		GO

		ALTER TABLE [dbo].[System_Member_Discount_Type_Definition_new] ADD  CONSTRAINT [DF_System_Member_Discount_Type_Definition_new_BitArray]  DEFAULT ((0)) FOR [BitArray]
		GO

		INSERT INTO [dbo].[System_Member_Discount_Type_Definition_new]
			   ([Id]
			   ,[Description]
			   ,[IsDeleted]
			   ,[ModifiedDate]
			   ,[BitArray])
		 SELECT [Id]
			   ,[Description]
			   ,[IsDeleted]
			   ,[ModifiedDate]
			   ,[BitArray]
			   FROM [dbo].[System_Member_Discount_Type_Definition]

		-- drop the existing table
		DROP TABLE [dbo].[System_Member_Discount_Type_Definition]

		-- rename the new one to replace the old one
		EXEC sp_rename '[dbo].[System_Member_Discount_Type_Definition_new]', 'System_Member_Discount_Type_Definition'

		COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
		THROW
	END CATCH
```

---

Finally, save your script in the xpressweigh.sql.scripts repository, under the XpressWeigh folder.  Call the script `ALTER TABLE [dbo].[TableName].sql` e.g. `ALTER TABLE [dbo].[System_Member_Discount_Type_Definition].sql`.

Submit your changes in a pull request.

## ⚠️ Important: Next steps ##

You must restart the switchboard server before the schema changes will be seen, as the switchboard server caches the database schema.

You must also update the XpressWeigh table object classes, so the handshake and upload process has knowledge of the new schema changes.

- [Click here for the VB `TableObject` documentation]()
- [Click here for the C# `TableObject` documentation]()